import rospy
from geometry_msgs.msg import Vector3
from hj_msgs.msg import Valve3, Servo3, Obstacle
from sensor_msgs.msg import Image

import cv2
from cv_bridge import CvBridge

from PyQt5 import QtCore, QtGui


class TopicMonitor(QtCore.QObject):
    sig_flowRate = QtCore.pyqtSignal(float, float, float, name='sig_flowRate')
    sig_valveCommands = QtCore.pyqtSignal(float, float, float, name='sig_valveCommands')
    sig_image = QtCore.pyqtSignal(QtGui.QImage, name='sig_image')
    sig_obstacle = QtCore.pyqtSignal()

    def __init__(self):
        QtCore.QObject.__init__(self) # initialisation required for object inheritance

        self.valveCommands_ok = False
        self.valveCommunication_ok = False
        self.flowRate_ok = False
        self.image_ok = False
        self.obstacle_ok = False

        rospy.Subscriber("/valve_commands", Valve3, self.callbackValves)
        rospy.Subscriber("/valve_status", Servo3, self.callbackValvesStatus)
        rospy.Subscriber("/measured_flow", Vector3, self.callbackFlowrate)
        rospy.Subscriber("/hj_camera/image_raw", Image, self.callbackImage)
        rospy.Subscriber("/obstacle", Obstacle, self.callbackObstacle)

    def callbackValvesStatus(self, msg):
        self.valveCommunication_ok = True

    def callbackValves(self, msg):
        self.valveCommands_ok = True
        self.sig_valveCommands.emit(msg.commands.x, msg.commands.y, msg.commands.z)

    def callbackFlowrate(self, msg):
        self.flowRate_ok = True
        self.sig_flowRate.emit(msg.x, msg.y, msg.z)

    def callbackImage(self, msg):
        bridge = CvBridge()
        cv_image = bridge.imgmsg_to_cv2(msg, desired_encoding='rgb8')
        height, width, bytesPerComponent = cv_image.shape
        bytesPerLine = bytesPerComponent * width;
        q_image = QtGui.QImage(cv_image.data, width, height, bytesPerLine, QtGui.QImage.Format_RGB888)
        self.image_ok = True
        self.sig_image.emit(q_image)

    def callbackObstacle(self, msg):
        self.obstacle_ok = True
        self.sig_obstacle.emit()
