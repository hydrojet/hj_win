from PyQt5 import QtWidgets


class GuiColors:
    def __init__(self):
        self.progressBar_ok = """
        QProgressBar {
            border: 1px solid rgb(65, 72, 85);
            border-radius: 7px;
            background-color: rgb(65, 72, 85);
        }

        QProgressBar::chunk {
            background-color: rgb(143, 255, 152);
        } """

        self.progressBar_warn = """
        QProgressBar {
            border: 1px solid rgb(65, 72, 85);
            border-radius: 7px;
            background-color: rgb(65, 72, 85);
        }

        QProgressBar::chunk {
            background-color: rgb(255, 255, 146);
        } """

        self.progressBar_critical = """
        QProgressBar {
            border: 1px solid rgb(65, 72, 85);
            border-radius: 7px;
            background-color: rgb(65, 72, 85);
        }

        QProgressBar::chunk {
            background-color: rgb(255, 99, 101);
        } """

        self.collision = """
        QLabel {
            background-color: rgba(255, 0, 0, 255);
        }"""

        self.no_collision = """
        QLabel {
            background-color: rgba(255, 255, 255, 0);
        }"""

