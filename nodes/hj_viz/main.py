import sys
import os
import time

from PyQt5 import QtCore
from PyQt5 import QtGui, QtWidgets
from PyQt5 import uic

import rospy

from topic_monitor import TopicMonitor
from gui_colors import GuiColors


class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        self.colors = GuiColors()

        # Load App FORM
        path = os.path.join(os.path.dirname(__file__), "form.ui")
        self.ui = uic.loadUi(path, self)

        # Splash Screen
        pathSplashImage = os.path.join(os.path.dirname(__file__), "images/splash.svg")
        pixmap = QtGui.QPixmap(pathSplashImage)
        self.splash = QtWidgets.QSplashScreen(pixmap, QtCore.Qt.WindowStaysOnTopHint)
        self.splash.setMask(pixmap.mask())
        self.splash.show()

        # Setup ROS Features
        self.splash.showMessage("Initializing ROS Environment", QtCore.Qt.AlignHCenter, QtCore.Qt.white)
        rospy.init_node('hj_viz', anonymous=True)

        # Subscribing to necessary topics
        self.splash.showMessage("Subscribing to Topics", QtCore.Qt.AlignHCenter, QtCore.Qt.white)
        self.monitor = TopicMonitor()

        # WatchDog Timer
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.callbackWatchDog)
        self.timer.start(1000)

        # Events
        self.splash.showMessage("Connecting Signals", QtCore.Qt.AlignHCenter, QtCore.Qt.white)
        self.connectEvents()

        # Configuration Done
        self.splash.showMessage("Done", QtCore.Qt.AlignHCenter, QtCore.Qt.white)

    def callbackWatchDog(self):
        self.watchdogCheckTopic(self.ui.radioButton_valveCommands, self.monitor.valveCommands_ok)
        self.watchdogCheckTopic(self.ui.radioButton_valveCommunication, self.monitor.valveCommunication_ok)
        self.watchdogCheckTopic(self.ui.radioButton_flowRate, self.monitor.flowRate_ok)
        self.watchdogCheckTopic(self.ui.radioButton_camera, self.monitor.image_ok)
        self.watchdogCheckTopic(self.ui.radioButton_obstacle, self.monitor.obstacle_ok)
        self.monitor.valveCommands_ok = False
        self.monitor.valveCommunication_ok = False
        self.monitor.flowRate_ok = False
        self.monitor.image_ok = False
        self.monitor.obstacle_ok = False
        # Reset Image container background color
        self.ui.container_image.setStyleSheet(self.colors.no_collision)

    def watchdogCheckTopic(self, radioBtn, flag):
        if radioBtn.isChecked():
            if not flag:
                radioBtn.setChecked(False)
        else:
            if flag:
                radioBtn.setChecked(True)

    def connectEvents(self):
        self.ui.button_quit.clicked.connect(QtCore.QCoreApplication.quit)
        self.monitor.sig_flowRate.connect(self.updateFlowrate)
        self.monitor.sig_valveCommands.connect(self.updateValves)
        self.monitor.sig_image.connect(self.updateImage)
        self.monitor.sig_obstacle.connect(self.obstacleWarn)

    def updateValves(self, x, y, z):
        self.thresholdValve(x, self.ui.progressBar_valve1)
        self.ui.progressBar_valve1.setValue(x * 10.0)
        self.thresholdValve(y, self.ui.progressBar_valve2)
        self.ui.progressBar_valve2.setValue(y * 10.0)
        self.thresholdValve(z, self.ui.progressBar_valve3)
        self.ui.progressBar_valve3.setValue(z * 10.0)
        self.ui.lcdNumber_valve1.display(x)
        self.ui.lcdNumber_valve2.display(y)
        self.ui.lcdNumber_valve3.display(z)

    def updateFlowrate(self, x, y, z):
        self.thresholdFlowrate(x, self.ui.progressBar_flow1)
        self.ui.progressBar_flow1.setValue(x * 1000.0)
        self.thresholdFlowrate(y, self.ui.progressBar_flow2)
        self.ui.progressBar_flow2.setValue(y * 1000.0)
        self.thresholdFlowrate(z, self.ui.progressBar_flow3)
        self.ui.progressBar_flow3.setValue(z * 1000.0)
        self.ui.lcdNumber_flow1.display(x)
        self.ui.lcdNumber_flow2.display(y)
        self.ui.lcdNumber_flow3.display(z)

    def updateImage(self, q_image):
        self.ui.container_image.setPixmap(QtGui.QPixmap.fromImage(q_image).scaled(800, 800, QtCore.Qt.KeepAspectRatio))

    def obstacleWarn(self):
        self.ui.container_image.setStyleSheet(self.colors.collision)

    def thresholdValve(self, value, bar):
        if 0.0 < value <= 80.0:
            bar.setStyleSheet(self.colors.progressBar_ok)
        elif 80.0 < value <= 90.0:
            bar.setStyleSheet(self.colors.progressBar_warn)
        else:
            bar.setStyleSheet(self.colors.progressBar_critical)

    def thresholdFlowrate(self, value, bar):
        if 0.0 < value <= 0.8:
            bar.setStyleSheet(self.colors.progressBar_ok)
        elif 0.8 < value <= 1.2:
            bar.setStyleSheet(self.colors.progressBar_warn)
        else:
            bar.setStyleSheet(self.colors.progressBar_critical)


app = QtWidgets.QApplication(sys.argv)
app.processEvents()

# Simulate something that takes time
time.sleep(1)

window = MainWindow()
window.setWindowIcon(QtGui.QIcon('images/hj_logo.svg'))
window.showFullScreen()
#window.show()
window.splash.finish(window)
app.exec_()
