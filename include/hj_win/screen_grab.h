/*
 * Created by Simone 07/10/2019
 */

#ifndef SCREEN_GRAB_SCREEN_GRAB_H
#define SCREEN_GRAB_SCREEN_GRAB_H

#include <iostream>
#include <windows.h>

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"


class ScreenGrabber {

	public:

		ScreenGrabber(int offset_x, int offset_y, int x_len, int y_len);
		~ScreenGrabber();
		void grabToClipboard();
		void grabToCV_beta(cv::Mat& matBitmap);
		void grabToCV_view();
		void grabToCV(cv::Mat& src);

	private:

		POINT a;
		POINT b;
		int x_size = 0;
		int y_size = 0;
		HDC     hScreen;
		HDC		hdc;
		HDC     hDC;
		HBITMAP hBitmap;
		HGDIOBJ old_obj;
		BOOL    bRet;
		void* ptrBitmapPixels; // <-- Pointer variable that will contain the potinter for the pixels
		BITMAPINFO bi;

		HWND hwnd = GetDesktopWindow();
		HDC hwindowDC;
		HDC hwindowCompatibleDC;
		HBITMAP hbwindow;
		BITMAPINFOHEADER  bih;
		RECT windowsize;

};

#endif  // SCREEN_GRAB_SCREEN_GRAB_H
