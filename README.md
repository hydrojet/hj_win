Compile third party software using Visual Studio (2019) Compiler: Debug Win32

# ROS Package hj_win #

## This node implements ROS and non-ROS nodes for running on Windows machines
### The repository implements:
- camera_commands: a ROS Subscriber that reads a CameraCommands topic and sends the commands to the camera through a non-ROS windows socket.
- camera_publisher: a ROS Publisher that receives camera frames from a non-ROS windows socket and publishes them to a /hj_camera/image_raw topic.
- screen_grab_node: standalone /hj_camera/image_raw ROS publisher that grabs the portion of the screen where the LYA app shows the camera feed
- WinServer: Visual Studio based solution that impelments a window socket server. It connescts to the camera using a USB interface and to the ROS nodes using a TCP/IP interface. Such a software architecture with ROS nodes communicating with a non-ROS socket is needed because of different building requirements for the scripts (ROS uses an amd64 incompatible with the LYA_API.dll library)


## Software requirements:
- ROS Windows ([link](http://wiki.ros.org/Installation/Windows))
	Additional setup:
    - In "C:\opt\ros\melodic\x64\setup.bat" add:
    	++ If the windows machine is used inside the HJ ROS network with the master running on another machine++
        set ROS_IP=169.254.124.59
        set ROS_MASTER_URI=http://169.254.124.1:11311
		++else++
        set ROS_IP= machine ip from ipconfig
        set ROS_MASTER_URI=http://localhost:11311
		++ in both cases:++
        C:\catkin_ws\devel\setup.bat
	- Run "C:\opt\ros\melodic\x64\setup.bat" in an open terminal to source the ROS environment.
- Visual Studio (tested: 2019)

## Build and Deploy
Compile third party software (WinServer) using Visual Studio (2019) Compiler: Debug Win32
Use standard catkin_make for ROS nodes

## Launch Files
==screen_grab.launch== lauches screen_grab_node and creates ROS parameters such as the position and size of the ROI
```sh
roslaunch hj_win screen_grab.launch
```
==camera_manager.launch== lauches camera_commands and camera_publisher nodes. It also creates ROS parameters needed to setup the network interface with the socket. Each ROS node/client connects with WinServer on custom ports in order to make it recognizable by the socket and stream the data to the correct client. Be sure WinServer is running before launching camera_manager.
```sh
roslaunch hj_win camera_manager.launch
```

## WinServer
Launch the socket using the Visual Studio IDE or just using the .exe file.

## Authors
#### Simone Calo (s.calo1@leeds.ac.uk)
