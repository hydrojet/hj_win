/*
 * Created by Simone 31/01/2020
 */

#include <iostream>
#include <windows.h>
#include <Libloaderapi.h>

#define CONTROL_MODE 1
#define CONTROL_TARGET 2
#define CONTROL_SPEED 3
#define AWB 4
#define FPN 5
#define NOISE_REDUCTION 6
#define EDGE_ENHANCEMENT 7
#define EXPOSURE_TIME 8

 /**
 Fujikura guidelines for SDK usage
	 1.Initialize connection [LYA_Init()]
	 2.Verify that camera is connected [LYA_GetStatus()]
	 3.Create handler for new incoming data [LYA_GetDataSemaphoHandle()]
	 4.Acquire data [LYA_GetFrame()] *Whenever a new frame becomes available the handler gets notified
	 5.Terminate connection [LYA_Term()]
 **/
class LYA_API {
public:
	/* Construtors and Destructor */
	LYA_API();
	LYA_API(std::string path_to_dll);
	~LYA_API();

	// Image Acquisition Essentials
	bool LYA_Init();
	bool LYA_Term();
	bool LYA_GetStatus(DWORD*);
	bool LYA_GetDataSemaphoHandle();
	bool LYA_GetFrame(BYTE*, DWORD, DWORD*);

	// Camera Settings
	bool LYA_Set_BlackLevel(int);
	bool LYA_Get_BlackLevel(int*);
	bool LYA_Set_ControlMode(int, int);
	bool LYA_Get_ControlMode(int, int*);
	bool LYA_Set_ExposureTime(int);
	bool LYA_Get_ExposureTime(int*);
	bool LYA_Set_RawGain(int);
	bool LYA_Get_RawGain(int*);
	bool LYA_Set_ControlSpeed(int);
	bool LYA_Get_ControlSpeed(int*);
	bool LYA_Set_AWB(int);
	bool LYA_Get_AWB(int*);
	bool LYA_Set_FPN_Corr(int);
	bool LYA_Get_FPN_Corr(int*);
	bool LYA_Set_NoiseReduction(int);
	bool LYA_Get_NoiseReduction(int*);
	bool LYA_Set_EdgeEnhancement(int);
	bool LYA_Get_EdgeEnhancement(int*);
	bool LYA_Set_ControlTarget(int);
	bool LYA_Get_ControlTarget(int*);
	bool LYA_Set_DataFormat(int);
	bool LYA_Get_DataFormat(int*);


	/* LYA_API.dll functions prototypes */
	//int LYA_Init(void)
	typedef INT(CALLBACK* FUN1)();
	//int LYA_GetStatus(DWORD * pdwFlag)
	typedef INT(CALLBACK* FUN2)(DWORD*);
	//int LYA_GetDataSemaphoHandle(HANDLE *phDataSem)
	typedef INT(CALLBACK* FUN3)(HANDLE*);
	//int LYA_Get_BlackLevel(int* pnNum)
	typedef INT(CALLBACK* FUN4)(int*);
	//int LYA_GetFrame(BYTE* pBuf, DWORD dwSize, DWORD* pdwWSize)
	typedef INT(CALLBACK* FUN5)(BYTE*, DWORD, DWORD*);
	//int LYA_Set_ControlMode(int nType, int nNum)
	typedef INT(CALLBACK* FUN6)(int, int);
	//int LYA_Get_ControlMode(int nType, int nNum)
	typedef INT(CALLBACK* FUN7)(int, int*);
	//int LYA_Set_BlackLevel(int pnNum)
	typedef INT(CALLBACK* FUN8)(int);

	HANDLE incoming_frame_handle;

private:
	HINSTANCE hDLL;
	FUN1 lya_init, lya_term;
	FUN2 lya_get_status;
	FUN3 lya_get_data_semapho_handle;

	FUN5 lya_get_frame;
	FUN6 lya_set_controlmode;
	FUN7 lya_get_controlmode;
	FUN8 lya_set_black_level, lya_set_exposuretime, lya_set_rawgain, lya_set_controlspeed, lya_set_awb, lya_set_fpn_corr, lya_set_noisereduction, lya_set_edgeenhancement, lya_set_controltarget, lya_set_dataformat;
	FUN4 lya_get_black_level, lya_get_exposuretime, lya_get_rawgain, lya_get_controlspeed, lya_get_awb, lya_get_fpn_corr, lya_get_noisereduction, lya_get_edgeenhancement, lya_get_controltarget, lya_get_dataformat;

	INT init_status;
	LONG wait_for_frame;
	bool success_init = false;
	bool success_connection = false;
	unsigned long int time_before;
	unsigned long int time_after;
};
