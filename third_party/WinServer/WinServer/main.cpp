/*
 * Created by Simone 31/01/2020
 */

#include <winsock.h>

#include <stdlib.h>
#include <iostream>


 //// Prototypes ////////////////////////////////////////////////////////
extern int DoWinsock(const char* pcHost, int nPort);


//// Constants /////////////////////////////////////////////////////////
// Default port to connect to on the server
const int kDefaultServerPort = 27015;


//// main //////////////////////////////////////////////////////////////
int main(int argc, char* argv[]) {

	// Get host and (optionally) port from the command line
	const char* pcHost = argv[1];
	int nPort = kDefaultServerPort;
	if (argc >= 3) {
		nPort = atoi(argv[2]);
	}

	// Do a little sanity checking .
	int nNumArgsIgnored = (argc - 3);
	if (nNumArgsIgnored > 0) {
		std::cerr << nNumArgsIgnored << " extra argument" << (nNumArgsIgnored == 1 ? "" : "s") << " ignored.  FYI." << std::endl;
	}

	// Start Winsock up
	WSAData wsaData;
	int nCode;
	if ((nCode = WSAStartup(MAKEWORD(1, 1), &wsaData)) != 0) {
		std::cerr << "WSAStartup() returned error code " << nCode << "." << std::endl;
		return 255;
	}

	// Get the local hostname
	char szHostName[255];
	gethostname(szHostName, 255);
	struct hostent* host_entry;
	host_entry = gethostbyname(szHostName);
	char* szLocalIP;
	szLocalIP = inet_ntoa(*(struct in_addr*) * host_entry->h_addr_list);

	if (argc < 2) {
		std::cout << "Retrieving host name and ip address" << std::endl;
		pcHost = szLocalIP;
		std::cout << "Using Server settings:\nHost Name::\t" << szHostName << "\nHost Entry:\t" << host_entry << "\nIP:\t\t" << szLocalIP << std::endl;
	}

	// Call the main example routine.
	int retval = DoWinsock(pcHost, nPort);

	// Shut Winsock back down and take off.
	WSACleanup();
	return retval;
}

