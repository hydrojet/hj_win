/*
 * Created by Simone 31/01/2020
 */

#include "ws-util.h"
#include "lyalib.h"
#include <winsock.h>
#include <iostream>
#include <vector>
#include <string>
#include <sstream>


 //// Constants /////////////////////////////////////////////////////////
#define IMG_ROWS 404
#define IMG_COLS 404
#define DEFAULT_BUFLEN (IMG_ROWS * IMG_COLS * 4)
#define SEND_IMG_ROWS 400
#define SEND_IMG_COLS 400
#define SEND_DEFAULT_BUFLEN (SEND_IMG_ROWS * SEND_IMG_COLS * 2) 
const int kBufferSize = 10;

LYA_API camera("LYA_API.dll");


//// Prototypes ////////////////////////////////////////////////////////

SOCKET SetUpListener(const char* pcAddress, int nPort);
void AcceptConnections(SOCKET ListeningSocket);
bool EchoIncomingPackets(SOCKET sd);
bool SendImage(SOCKET sd);
DWORD WINAPI ImageHandler(void* sd_);
bool ReceiveCommands(SOCKET sd);
DWORD WINAPI CommandsHandler(void* sd_);

//// DoWinsock /////////////////////////////////////////////////////////

int DoWinsock(const char* pcAddress, int nPort) {

	DWORD connection_status = 0;

	if (!camera.LYA_Init()) {
		std::cout << "[ERROR][LYA_API] Shutting down (init)" << std::endl;
		return false;
	}
	if (!camera.LYA_GetStatus(&connection_status)) {
		std::cout << "[ERROR][LYA_API] Shutting down (connection)" << std::endl;
		return false;
	}

	std::cout << "[INFO][SERVER] Establishing the listener..." << std::endl;
	SOCKET ListeningSocket = SetUpListener(pcAddress, htons(nPort));
	if (ListeningSocket == INVALID_SOCKET) {
		std::cout << std::endl << WSAGetLastErrorMessage("establish listener") << std::endl;
		return 3;
	}

	std::cout << "[INFO][SERVER] Waiting for connections..." << std::endl << std::flush;

	while (1) {
		AcceptConnections(ListeningSocket);
		std::cout << "[INFO][SERVER] Acceptor restarting..." << std::endl;
	}

#if defined(_MSC_VER)
	return 0;
#endif
}


//// SetUpListener /////////////////////////////////////////////////////
// Sets up a listener on the given interface and port, returning the
// listening socket if successful; if not, returns INVALID_SOCKET.

SOCKET SetUpListener(const char* pcAddress, int nPort)
{
	u_long nInterfaceAddr = inet_addr(pcAddress);
	if (nInterfaceAddr != INADDR_NONE) {
		SOCKET sd = socket(AF_INET, SOCK_STREAM, 0);
		if (sd != INVALID_SOCKET) {
			sockaddr_in sinInterface;
			sinInterface.sin_family = AF_INET;
			sinInterface.sin_addr.s_addr = nInterfaceAddr;
			sinInterface.sin_port = nPort;
			if (bind(sd, (sockaddr*)&sinInterface, sizeof(sockaddr_in)) != SOCKET_ERROR) {
				listen(sd, SOMAXCONN);
				return sd;
			}
			else {
				std::cerr << WSAGetLastErrorMessage("bind() failed") << std::endl;
			}
		}
	}

	return INVALID_SOCKET;
}


//// EchoHandler ///////////////////////////////////////////////////////
// Handles the incoming data by reflecting it back to the sender.

DWORD WINAPI EchoHandler(void* sd_)
{
	int nRetval = 0;
	SOCKET sd = (SOCKET)sd_;

	if (!EchoIncomingPackets(sd)) {
		std::cerr << std::endl << WSAGetLastErrorMessage("Echo incoming packets failed") << std::endl;
		nRetval = 3;
	}

	std::cout << "Shutting connection down..." << std::flush;
	if (ShutdownConnection(sd)) {
		std::cout << "Connection is down." << std::endl;
	}
	else {
		std::cerr << std::endl << WSAGetLastErrorMessage("Connection shutdown failed") << std::endl;
		nRetval = 3;
	}

	return nRetval;
}


//// AcceptConnections /////////////////////////////////////////////////
// Spins forever waiting for connections.  For each one that comes in, 
// we create a thread to handle it and go back to waiting for
// connections.  If an error occurs, we return.

void AcceptConnections(SOCKET ListeningSocket)
{
	sockaddr_in sinRemote;
	int nAddrSize = sizeof(sinRemote);

	while (1) {
		SOCKET sd = accept(ListeningSocket, (sockaddr*)&sinRemote, &nAddrSize);
		if (sd != INVALID_SOCKET) {
			std::cout << "[INFO][SERVER] Accepted connection from " << inet_ntoa(sinRemote.sin_addr) << ":" << ntohs(sinRemote.sin_port) << "." << std::endl;

			DWORD nThreadID;
			if (ntohs(sinRemote.sin_port) == 55379) {
				CreateThread(0, 0, CommandsHandler, (void*)sd, 0, &nThreadID);
				std::cout << "[INFO][SERVER] Commands Subscriber connected!" << std::endl;
			}
			else{// (ntohs(sinRemote.sin_port) == 44268) {
				CreateThread(0, 0, ImageHandler, (void*)sd, 0, &nThreadID);
				std::cout << "[INFO][SERVER] Image Publisher connected!" << std::endl;
			}
		}
		else {
			std::cerr << WSAGetLastErrorMessage("accept() failed") << std::endl;
			return;
		}
	}
}


//// EchoIncomingPackets ///////////////////////////////////////////////
// Bounces any incoming packets back to the client.  We return false
// on errors, or true if the client closed the socket normally.

bool EchoIncomingPackets(SOCKET sd) {
	// Read data from client
	char acReadBuffer[kBufferSize];
	int nReadBytes;
	do {
		nReadBytes = recv(sd, acReadBuffer, kBufferSize, 0);
		if (nReadBytes > 0) {
			std::cout << "Received " << nReadBytes <<
				" bytes from client." << std::endl;

			int nSentBytes = 0;
			while (nSentBytes < nReadBytes) {
				int nTemp = send(sd, acReadBuffer + nSentBytes,
					nReadBytes - nSentBytes, 0);
				if (nTemp > 0) {
					std::cout << "Sent " << nTemp <<
						" bytes back to client." << std::endl;
					nSentBytes += nTemp;
				}
				else if (nTemp == SOCKET_ERROR) {
					return false;
				}
				else {
					// Client closed connection before we could reply to
					// all the data it sent, so bomb out early.
					std::cout << "Peer unexpectedly dropped connection!" <<
						std::endl;
					return true;
				}
			}
		}
		else if (nReadBytes == SOCKET_ERROR) {
			return false;
		}
	} while (nReadBytes != 0);

	std::cout << "Connection closed by peer." << std::endl;
	return true;
}

//// SendImage ///////////////////////////////////////////////

bool SendImage(SOCKET sd) {

	INT init_status;
	INT black_level;
	HANDLE incoming_frame_handle;
	BYTE incoming_frame[DEFAULT_BUFLEN];
	DWORD incoming_frame_expected_size = DEFAULT_BUFLEN;
	DWORD incoming_frame_received_size = 0;
	unsigned long int fps;
	std::vector<uint8_t> image(SEND_DEFAULT_BUFLEN);
	int count = 0, count_trim = 0;
	int nTemp;

	camera.LYA_GetDataSemaphoHandle();

	while (true) {

		camera.LYA_GetFrame(incoming_frame, incoming_frame_expected_size, &incoming_frame_received_size);
		incoming_frame_received_size = 0;

		for (int i = 0; i < DEFAULT_BUFLEN; i = i + 8) {
			nTemp = count_trim % (IMG_ROWS/2);
			count_trim++;
			if (i < ((IMG_ROWS * 1) * 8) || i > DEFAULT_BUFLEN - ((IMG_ROWS * 1) * 8)) {
				//std::cout << "[ERROR][SERVER] Edge pixels" << std::endl;
			}
			else if (nTemp != 0 && nTemp != 201) {
				//std::cout << "Count: " << count << "\ti: " << i << std::endl;
				image[count] = incoming_frame[i + 1];
				image[count + 1] = incoming_frame[i + 3];
				image[count + 2] = incoming_frame[i + 5];
				image[count + 3] = incoming_frame[i + 7];
				count = count + 4;
			}
			else {
				//std::cout << "[ERROR][SERVER] Edge pixels" << std::endl;
			}
		}
		count = 0;
		count_trim = 0;

		nTemp = send(sd, (char *) &image.front(), SEND_DEFAULT_BUFLEN, 0);
		if (nTemp > 0) {
			//std::cout << "[INFO][SERVER] Sent " << nTemp << " bytes to ROS client." << std::endl;
		}
		else if (nTemp == SOCKET_ERROR) {
			std::cout << "[ERROR][SERVER] Socket Error!" << std::endl;
			return false;
		}
		else {
			// Client closed connection
			std::cout << "[ERROR][SERVER] Image publisher client unexpectedly dropped connection!" << std::endl;
			return true;
		}

	}
}

//// ImageHandler ///////////////////////////////////////////////////////

DWORD WINAPI ImageHandler(void* sd_) {
	int nRetval = 0;
	SOCKET sd = (SOCKET)sd_;

	if (!SendImage(sd)) {
		std::cerr << std::endl << WSAGetLastErrorMessage("[ERROR][SERVER] Sending Image packets failed") << std::endl;
		nRetval = 3;
	}

	std::cout << "[INFO][SERVER] Shutting connection down..." << std::flush;

	if (ShutdownConnection(sd)) {
		std::cout << "[INFO][SERVER] Connection is down." << std::endl;
	}
	else {
		std::cerr << std::endl << WSAGetLastErrorMessage("[ERROR][SERVER] Connection shutdown failed") << std::endl;
		nRetval = 3;
	}

	return nRetval;
}

//// ReceiveCommands ///////////////////////////////////////////////
bool ReceiveCommands(SOCKET sd) {
	// Read data from client
	char acReadBuffer[kBufferSize];
	int nReadBytes;
	int control_speed, control_target;
	int value;

	camera.LYA_Get_ControlTarget(&control_target);
	camera.LYA_Get_ControlSpeed(&control_speed);

	camera.LYA_Get_FPN_Corr(&value);
	std::cout << "Current FPN: " << value << std::endl;
	camera.LYA_Get_NoiseReduction(&value);
	std::cout << "Current Noise Reduction: " << value << std::endl;
	camera.LYA_Get_EdgeEnhancement(&value);
	std::cout << "Current Edge Enhancement: " << value << std::endl;
	camera.LYA_Get_ExposureTime(&value);
	std::cout << "Current Exposure Time: " << value << std::endl;

	do {
		nReadBytes = recv(sd, acReadBuffer, kBufferSize, 0);
		if (nReadBytes > 0) {

			std::vector<int> result;
			std::stringstream ss;
			std::string item;
			ss << acReadBuffer;

			while (getline(ss, item, '*')) {
				try {
					result.push_back(stof(item));
				}
				catch (std::invalid_argument & e) {
					result.push_back(-1);
					std::cout << "[ERROR][SERVER] Communication error in recieving commands" << std::endl;
					continue;
				}
			}

			switch (result[0]) {
			case 0:
				std::cout << "[INFO][SERVER] Starting commands listener" << std::endl;
				break;
			case CONTROL_MODE:
				camera.LYA_Set_ControlMode(result[1], result[2]);
				break;
			case CONTROL_TARGET:
				control_target = control_target + result[2];
				if (control_speed >= 0 && control_speed <= 4095) {
					camera.LYA_Set_ControlTarget(control_target);
				}
				else if (control_target > 4095)
					control_target = 4095;
				else
					control_target = 0;
				break;
			case CONTROL_SPEED:
				control_speed = control_speed + result[2];
				if (control_speed >= 0 && control_speed <= 7) {
					camera.LYA_Set_ControlSpeed(control_speed);
				}
				else if (control_speed > 7)
					control_speed = 7;
				else
					control_speed = 0;
				break;
			case AWB:
				camera.LYA_Set_AWB(result[2]);
				break;
			case FPN:
				camera.LYA_Set_FPN_Corr(result[2]);
				break;
			case NOISE_REDUCTION:
				camera.LYA_Set_NoiseReduction(result[2]);
				break;
			case EDGE_ENHANCEMENT:
				camera.LYA_Set_EdgeEnhancement(result[2]);
				break;
			case EXPOSURE_TIME:
				camera.LYA_Set_ExposureTime(result[2]);
				break;
			default:
				std::cout << "[NFO][SERVER] Invalid camera command" << std::endl;
				break;
			}

		}
		else if (nReadBytes == SOCKET_ERROR) {
			return false;
		}
	} while (nReadBytes >= 0);

	std::cout << "Connection closed by peer." << std::endl;
	return true;
}

//// CommandsHandler ///////////////////////////////////////////////////////
DWORD WINAPI CommandsHandler(void* sd_) {
	int nRetval = 0;
	SOCKET sd = (SOCKET)sd_;

	if (!ReceiveCommands(sd)) {
		std::cerr << std::endl << WSAGetLastErrorMessage("[ERROR][SERVER] GEtting commands packets failed") << std::endl;
		nRetval = 3;
	}

	std::cout << "[INFO][SERVER] Shutting connection down..." << std::flush;

	if (ShutdownConnection(sd)) {
		std::cout << "[INFO][SERVER] Connection is down." << std::endl;
	}
	else {
		std::cerr << std::endl << WSAGetLastErrorMessage("[ERROR][SERVER] Connection shutdown failed") << std::endl;
		nRetval = 3;
	}

	return nRetval;
}

