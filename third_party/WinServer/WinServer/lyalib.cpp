/*
 * Created by Simone 31/01/2020
 */

#include "lyalib.h"

LYA_API::LYA_API() {
	hDLL = LoadLibraryA("C:\\Users\\elsc\\source\\repos\\LYA\\LYA_API.dll");
}

LYA_API::LYA_API(std::string path_to_dll) {
	hDLL = LoadLibraryA("LYA_API.dll");
}

LYA_API::~LYA_API() {

}

bool LYA_API::LYA_Term() {
	if (success_init) {
		init_status = lya_term();
		if (init_status == 0) {
			std::cout << "[ERROR][LYA_API] Terminating LYA!" << std::endl;
			return true;
		}
		else {
			std::cout << "[ERROR][LYA_API] Unable to terminate LYA!" << std::endl;
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] LYA hasn't been initialized yet" << std::endl;
		return false;
	}
}

bool LYA_API::LYA_Init() {
	if (hDLL != NULL) {
		/** LYA_Term **/
		lya_term = (FUN1)GetProcAddress(hDLL, "LYA_Term");
		if (!lya_term) {
			std::cout << "[ERROR][LYA_API] LYA_Term: no such function or method" << std::endl;
			FreeLibrary(hDLL);
			return false;
		}
		/** LYA_Init **/
		lya_init = (FUN1)GetProcAddress(hDLL, "LYA_Init");
		if (!lya_init) {
			std::cout << "[ERROR][LYA_API] LYA_Init: no such function or method" << std::endl;
			FreeLibrary(hDLL);
			return false;
		}
		else {
			init_status = lya_init();
			if (init_status == 0) {
				/* Succesfully initialized! **/
				success_init = true;
			}
			else {
				std::cout << "[ERROR][LYA_API] Unable to initialize the device" << std::endl;
				FreeLibrary(hDLL);
				return false;
			}
		}
		/** LYA_GetStatus **/
		lya_get_status = (FUN2)GetProcAddress(hDLL, "LYA_GetStatus");
		if (!lya_get_status) {
			std::cout << "[ERROR][LYA_API] LYA_GetStatus: no such function or method" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
		/** LYA_GetDataSemaphoHandle **/
		lya_get_data_semapho_handle = (FUN3)GetProcAddress(hDLL, "LYA_GetDataSemaphoHandle");
		if (!lya_get_data_semapho_handle) {
			std::cout << "[ERROR][LYA_API] LYA_GetDataSemaphoHandle: no such function or method" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
		/** LYA_GetFrame **/
		lya_get_frame = (FUN5)GetProcAddress(hDLL, "LYA_GetFrame");
		if (!lya_get_frame) {
			std::cout << "[ERROR][LYA_API] LYA_GetFrame: no such function or method" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
		/** LYA_Set_BlackLevel **/
		lya_set_black_level = (FUN8)GetProcAddress(hDLL, "LYA_Set_BlackLevel");
		if (!lya_set_black_level) {
			std::cout << "[ERROR][LYA_API] LYA_Set_BlackLevel: no such function or method" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
		/** LYA_Get_BlackLevel **/
		lya_get_black_level = (FUN4)GetProcAddress(hDLL, "LYA_Get_BlackLevel");
		if (!lya_get_black_level) {
			std::cout << "[ERROR][LYA_API] LYA_Get_BlackLevel: no such function or method" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
		/** LYA_Set_ControlMode(int, int) **/
		lya_set_controlmode = (FUN6)GetProcAddress(hDLL, "LYA_Set_ControlMode");
		if (!lya_set_controlmode) {
			std::cout << "[ERROR][LYA_API] LYA_Set_ControlMode: no such function or method" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
		/** LYA_Get_ControlMode(int, int*) **/
		lya_get_controlmode = (FUN7)GetProcAddress(hDLL, "LYA_Get_ControlMode");
		if (!lya_get_controlmode) {
			std::cout << "[ERROR][LYA_API] LYA_Get_ControlMode: no such function or method" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
		/** LYA_Set_ExposureTime(int) **/
		lya_set_exposuretime = (FUN8)GetProcAddress(hDLL, "LYA_Set_ExposureTime");
		if (!lya_set_exposuretime) {
			std::cout << "[ERROR][LYA_API] LYA_Set_ExposureTime: no such function or method" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
		/** LYA_Get_ExposureTime(int*) **/
		lya_get_exposuretime = (FUN4)GetProcAddress(hDLL, "LYA_Get_ExposureTime");
		if (!lya_get_exposuretime) {
			std::cout << "[ERROR][LYA_API] LYA_Get_ExposureTime: no such function or method" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
		/** LYA_Set_RawGain(int) **/
		lya_set_rawgain = (FUN8)GetProcAddress(hDLL, "LYA_Set_RawGain");
		if (!lya_set_rawgain) {
			std::cout << "[ERROR][LYA_API] LYA_Set_RawGain: no such function or method" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
		/** LYA_Get_RawGain(int*) **/
		lya_get_rawgain = (FUN4)GetProcAddress(hDLL, "LYA_Get_RawGain");
		if (!lya_get_rawgain) {
			std::cout << "[ERROR][LYA_API] LYA_Get_RawGain: no such function or method" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
		/** LYA_Set_ControlSpeed(int) **/
		lya_set_controlspeed = (FUN8)GetProcAddress(hDLL, "LYA_Set_ControlSpeed");
		if (!lya_set_controlspeed) {
			std::cout << "[ERROR][LYA_API] LYA_Set_ControlSpeed: no such function or method" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
		/** LYA_Get_ControlSpeed(int*) **/
		lya_get_controlspeed = (FUN4)GetProcAddress(hDLL, "LYA_Get_ControlSpeed");
		if (!lya_get_controlspeed) {
			std::cout << "[ERROR][LYA_API] LYA_Get_ControlSpeed: no such function or method" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
		/** LYA_Set_AWB(int) **/
		lya_set_awb = (FUN8)GetProcAddress(hDLL, "LYA_Set_AWB");
		if (!lya_set_awb) {
			std::cout << "[ERROR][LYA_API] LYA_Set_AWB: no such function or method" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
		/** LYA_Get_AWB(int*) **/
		lya_get_awb = (FUN4)GetProcAddress(hDLL, "LYA_Get_AWB");
		if (!lya_get_awb) {
			std::cout << "[ERROR][LYA_API] LYA_Get_AWB: no such function or method" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
		/** LYA_Set_FPN_Corr(int) **/
		lya_set_fpn_corr = (FUN8)GetProcAddress(hDLL, "LYA_Set_FPN_Corr");
		if (!lya_set_fpn_corr) {
			std::cout << "[ERROR][LYA_API] LYA_Set_FPN_Corr: no such function or method" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
		/** LYA_Get_FPN_Corr(int*) **/
		lya_get_fpn_corr = (FUN4)GetProcAddress(hDLL, "LYA_Get_FPN_Corr");
		if (!lya_get_fpn_corr) {
			std::cout << "[ERROR][LYA_API] LYA_Get_FPN_Corr: no such function or method" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
		/** LYA_Set_NoiseReduction(int) **/
		lya_set_noisereduction = (FUN8)GetProcAddress(hDLL, "LYA_Set_NoiseReduction");
		if (!lya_set_noisereduction) {
			std::cout << "[ERROR][LYA_API] LYA_Set_NoiseReduction: no such function or method" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
		/** LYA_Get_NoiseReduction(int*) **/
		lya_get_noisereduction = (FUN4)GetProcAddress(hDLL, "LYA_Get_NoiseReduction");
		if (!lya_get_noisereduction) {
			std::cout << "[ERROR][LYA_API] LYA_Get_NoiseReduction: no such function or method" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
		/** LYA_Set_EdgeEnhancement(int) **/
		lya_set_edgeenhancement = (FUN8)GetProcAddress(hDLL, "LYA_Set_EdgeEnhancement");
		if (!lya_set_edgeenhancement) {
			std::cout << "[ERROR][LYA_API] LYA_Set_EdgeEnhancement: no such function or method" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
		/** LYA_Get_EdgeEnhancement(int*) **/
		lya_get_edgeenhancement = (FUN4)GetProcAddress(hDLL, "LYA_Get_EdgeEnhancement");
		if (!lya_get_edgeenhancement) {
			std::cout << "[ERROR][LYA_API] LYA_Get_EdgeEnhancement: no such function or method" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
		/** LYA_Set_ControlTarget **/
		lya_set_controltarget = (FUN8)GetProcAddress(hDLL, "LYA_Set_ControlTarget");
		if (!lya_set_controltarget) {
			std::cout << "[ERROR][LYA_API] LYA_Set_ControlTarget: no such function or method" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
		/** LYA_Get_ControlTarget **/
		lya_get_controltarget = (FUN4)GetProcAddress(hDLL, "LYA_Get_ControlTarget");
		if (!lya_get_controltarget) {
			std::cout << "[ERROR][LYA_API] LYA_Get_ControlTarget: no such function or method" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
		/** LYA_Set_DataFormat **/
		lya_set_dataformat = (FUN8)GetProcAddress(hDLL, "LYA_Set_DataFormat");
		if (!lya_set_dataformat) {
			std::cout << "[ERROR][LYA_API] LYA_Set_DataFormat: no such function or method" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
		/** LYA_Get_DataFormat **/
		lya_get_dataformat = (FUN4)GetProcAddress(hDLL, "LYA_Get_DataFormat");
		if (!lya_get_dataformat) {
			std::cout << "[ERROR][LYA_API] LYA_Get_DataFormat: no such function or method" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] No LYA_API found" << std::endl;
		return false;
	}

	return true;
}

bool LYA_API::LYA_GetStatus(DWORD* connection_status) {
	if (success_init) {
		init_status = lya_get_status(connection_status);
		if (init_status == 0) {
			std::cout << "[INFO][LYA_API] Connecting to the camera ";
			while (*connection_status != 1) {
				init_status = lya_get_status(connection_status);
				std::cout << " .";
			}
			std::cout << "  Connected!\n";
			success_connection = true;
			return true;
		}
		else {
			std::cout << "[ERROR][LYA_API] Camera is not connected" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			success_connection = false;
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] LYA hasn't been initialized yet" << std::endl;
		return false;
	}
}

bool LYA_API::LYA_GetDataSemaphoHandle() {
	if (success_connection) {
		init_status = lya_get_data_semapho_handle(&incoming_frame_handle);
		if (init_status == 0) {
			return true;
		}
		else {
			std::cout << "[ERROR][LYA_API] Unable to create a callback" << std::endl;
			LYA_Term();
			FreeLibrary(hDLL);
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] LYA is not connected" << std::endl;
		return false;
	}
}

bool LYA_API::LYA_GetFrame(BYTE* incoming_frame, DWORD incoming_frame_expected_size, DWORD* incoming_frame_received_size) {
	if (success_connection) {
		wait_for_frame = WaitForSingleObject(incoming_frame_handle, INFINITE);
		if (wait_for_frame == WAIT_ABANDONED) {
			std::cout << "[ERROR][LYA_API] WaitForSingleObject: check for consistency" << std::endl;
		}
		else if (wait_for_frame == WAIT_OBJECT_0) {
			//std::cout << "[ERROR][LYA_API] ???" << std::endl;
		}
		else if (wait_for_frame == WAIT_TIMEOUT) {
			std::cout << "[ERROR][LYA_API] WaitForSingleObject timed out" << std::endl;
		}
		else if (wait_for_frame == WAIT_FAILED) {
			std::cout << "[ERROR][LYA_API] WaitForSingleObject failed" << std::endl;
		}

		init_status = lya_get_frame(incoming_frame, incoming_frame_expected_size, incoming_frame_received_size);

		if (init_status != 0) {
			std::cout << "[ERROR][LYA_API] Unable to get frames" << std::endl;
		}

		return true;
	}
	else {
		std::cout << "LYA is not connected" << std::endl;
		return false;
	}
}

bool LYA_API::LYA_Set_BlackLevel(int black_level) {
	if (black_level < 0 || black_level > 255) {
		std::cout << "[ERROR][LYA_API] Invalid values for setting black level" << std::endl;
		return false;
	}
	if (success_connection) {
		init_status = lya_set_black_level(black_level);
		if (init_status == 0) {
			return true;
		}
		else {
			std::cout << "[ERROR][LYA_API] Unable to set black level" << std::endl;
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] LYA is not connected" << std::endl;
		return false;
	}
}

bool LYA_API::LYA_Get_BlackLevel(int* black_level) {
	if (success_connection) {
		init_status = lya_get_black_level(black_level);
		if (init_status == 0) {
			return true;
		}
		else {
			std::cout << "[ERROR][LYA_API] Unable to get black level" << std::endl;
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] LYA is not connected" << std::endl;
		return false;
	}
}

bool LYA_API::LYA_Set_ControlMode(int mode, int value) {
	if (mode == 0 && (value < 0 || value > 3)) { // AEC/AGC Mode
		std::cout << "[ERROR][LYA_API] Invalid values for setting AEC/AGC mode" << std::endl;
		return false;
	}
	if (mode == 1 && (value < 0 || value > 1)) { // ALC Mode
		std::cout << "[ERROR][LYA_API] Invalid values for setting ALC mode" << std::endl;
		return false;
	}
	if (mode < 0 || mode > 1) {
		std::cout << "[ERROR][LYA_API] Invalid mode" << std::endl;
		return false;
	}
	if (success_connection) {
		init_status = lya_set_controlmode(mode, value);
		if (init_status == 0) {
			return true;
		}
		else {
			std::cout << "[ERROR][LYA_API] Unable to set control mode" << std::endl;
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] LYA is not connected" << std::endl;
		return false;
	}
}

bool LYA_API::LYA_Get_ControlMode(int mode, int* value) {
	if (mode < 0 || mode > 1) {
		std::cout << "[ERROR][LYA_API] Invalid mode" << std::endl;
		return false;
	}
	if (success_connection) {
		init_status = lya_get_controlmode(mode, value);
		if (init_status == 0) {
			return true;
		}
		else {
			std::cout << "[ERROR][LYA_API] Unable to get control mode" << std::endl;
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] LYA is not connected" << std::endl;
		return false;
	}
}

bool LYA_API::LYA_Set_ExposureTime(int value) {
	if (value < 1 || value > 438) {
		std::cout << "[ERROR][LYA_API] Invalid values for setting exposure time" << std::endl;
		return false;
	}
	if (success_connection) {
		init_status = lya_set_exposuretime(value);
		if (init_status == 0) {
			return true;
		}
		else {
			std::cout << "[ERROR][LYA_API] Unable to set exposure" << std::endl;
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] LYA is not connected" << std::endl;
		return false;
	}
}

bool LYA_API::LYA_Get_ExposureTime(int* value) {
	if (success_connection) {
		init_status = lya_get_exposuretime(value);
		if (init_status == 0) {
			return true;
		}
		else {
			std::cout << "[ERROR][LYA_API] Unable to get exposure" << std::endl;
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] LYA is not connected" << std::endl;
		return false;
	}
}

bool LYA_API::LYA_Set_RawGain(int value) {
	if (value < 16 || value > 255) {
		std::cout << "[ERROR][LYA_API] Invalid values for setting raw gain" << std::endl;
		return false;
	}
	if (success_connection) {
		init_status = lya_set_rawgain(value);
		if (init_status == 0) {
			return true;
		}
		else {
			std::cout << "[ERROR][LYA_API] Unable to set raw gain" << std::endl;
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] LYA is not connected" << std::endl;
		return false;
	}
}

bool LYA_API::LYA_Get_RawGain(int* value) {
	if (success_connection) {
		init_status = lya_get_rawgain(value);
		if (init_status == 0) {
			return true;
		}
		else {
			std::cout << "[ERROR][LYA_API] Unable to get raw gain" << std::endl;
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] LYA is not connected" << std::endl;
		return false;
	}
}

bool LYA_API::LYA_Set_ControlSpeed(int value) {
	if (value < 0 || value > 7) {
		std::cout << "[ERROR][LYA_API] Invalid values for setting control speed" << std::endl;
		return false;
	}
	if (success_connection) {
		init_status = lya_set_controlspeed(value);
		if (init_status == 0) {
			return true;
		}
		else {
			std::cout << "[ERROR][LYA_API] Unable to set control speed" << std::endl;
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] LYA is not connected" << std::endl;
		return false;
	}
}

bool LYA_API::LYA_Get_ControlSpeed(int* value) {
	if (success_connection) {
		init_status = lya_get_controlspeed(value);
		if (init_status == 0) {
			return true;
		}
		else {
			std::cout << "[ERROR][LYA_API] Unable to get control speed" << std::endl;
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] LYA is not connected" << std::endl;
		return false;
	}
}

bool LYA_API::LYA_Set_AWB(int value) {
	if (value < 0 || value > 1) {
		std::cout << "[ERROR][LYA_API] Invalid values for setting automatic white balance" << std::endl;
		return false;
	}
	if (success_connection) {
		init_status = lya_set_awb(value);
		if (init_status == 0) {
			int awb_flag = 0;
			LYA_Get_AWB(&awb_flag);
			std::cout << "[INFO][LYA_API] Automatic white balance running " << std::endl;
			while (awb_flag) {
				LYA_Get_AWB(&awb_flag);
				std::cout << " .";
			}
			std::cout << " --> Done!" << std::endl;
			return true;
		}
		else {
			std::cout << "[ERROR][LYA_API] Unable to set automatic white balance" << std::endl;
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] LYA is not connected" << std::endl;
		return false;
	}
}

bool LYA_API::LYA_Get_AWB(int* value) {
	if (success_connection) {
		init_status = lya_get_awb(value);
		if (init_status == 0) {
			return true;
		}
		else {
			std::cout << "[ERROR][LYA_API] Unable to get automatic white balance" << std::endl;
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] LYA is not connected" << std::endl;
		return false;
	}
}

bool LYA_API::LYA_Set_FPN_Corr(int value) {
	if (value < 0 || value > 3) {
		std::cout << "[ERROR][LYA_API] Invalid values for setting FPN correction" << std::endl;
		return false;
	}
	if (success_connection) {
		init_status = lya_set_fpn_corr(value);
		if (init_status == 0) {
			return true;
		}
		else {
			std::cout << "[ERROR][LYA_API] Unable to set FPN correction" << std::endl;
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] LYA is not connected" << std::endl;
		return false;
	}
}

bool LYA_API::LYA_Get_FPN_Corr(int* value) {
	if (success_connection) {
		init_status = lya_get_fpn_corr(value);
		if (init_status == 0) {
			return true;
		}
		else {
			std::cout << "[ERROR][LYA_API] Unable to get FPN correction" << std::endl;
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] LYA is not connected" << std::endl;
		return false;
	}
}

bool LYA_API::LYA_Set_NoiseReduction(int value) {
	if (value < 0 || value > 3) {
		std::cout << "[ERROR][LYA_API] Invalid values for setting noise reduction" << std::endl;
		return false;
	}
	if (success_connection) {
		init_status = lya_set_noisereduction(value);
		if (init_status == 0) {
			return true;
		}
		else {
			std::cout << "[ERROR][LYA_API] Unable to set noise reduction" << std::endl;
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] LYA is not connected" << std::endl;
		return false;
	}
}

bool LYA_API::LYA_Get_NoiseReduction(int* value) {
	if (success_connection) {
		init_status = lya_get_noisereduction(value);
		if (init_status == 0) {
			return true;
		}
		else {
			std::cout << "[ERROR][LYA_API] Unable to get noise reduction" << std::endl;
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] LYA is not connected" << std::endl;
		return false;
	}
}

bool LYA_API::LYA_Set_EdgeEnhancement(int value) {
	if (value < 0 || value > 3) {
		std::cout << "[ERROR][LYA_API] Invalid values for setting edge enhancement" << std::endl;
		return false;
	}
	if (success_connection) {
		init_status = lya_set_edgeenhancement(value);
		if (init_status == 0) {
			return true;
		}
		else {
			std::cout << "[ERROR][LYA_API] Unable to set edge enhancement" << std::endl;
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] LYA is not connected" << std::endl;
		return false;
	}
}

bool LYA_API::LYA_Get_EdgeEnhancement(int* value) {
	if (success_connection) {
		init_status = lya_get_edgeenhancement(value);
		if (init_status == 0) {
			return true;
		}
		else {
			std::cout << "[ERROR][LYA_API] Unable to get edge enhancement" << std::endl;
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] LYA is not connected" << std::endl;
		return false;
	}
}

bool LYA_API::LYA_Set_ControlTarget(int value) {
	if (value < 0 || value > 4095) {
		std::cout << "[ERROR][LYA_API] Invalid values for setting control target" << std::endl;
		return false;
	}
	if (success_connection) {
		init_status = lya_set_controltarget(value);
		if (init_status == 0) {
			return true;
		}
		else {
			std::cout << "[ERROR][LYA_API] Unable to set control target" << std::endl;
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] LYA is not connected" << std::endl;
		return false;
	}
}

bool LYA_API::LYA_Get_ControlTarget(int* value) {
	if (success_connection) {
		init_status = lya_get_controltarget(value);
		if (init_status == 0) {
			return true;
		}
		else {
			std::cout << "[ERROR][LYA_API] Unable to get control target" << std::endl;
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] LYA is not connected" << std::endl;
		return false;
	}
}

bool LYA_API::LYA_Set_DataFormat(int value) {
	if (value < 0 || value > 1) {
		std::cout << "[ERROR][LYA_API] Invalid values for setting data format" << std::endl;
		return false;
	}
	if (success_connection) {
		init_status = lya_set_dataformat(value);
		if (init_status == 0) {
			return true;
		}
		else {
			std::cout << "[ERROR][LYA_API] Unable to set data format" << std::endl;
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] LYA is not connected" << std::endl;
		return false;
	}
}

bool LYA_API::LYA_Get_DataFormat(int* value) {
	if (success_connection) {
		init_status = lya_get_dataformat(value);
		if (init_status == 0) {
			return true;
		}
		else {
			std::cout << "[ERROR][LYA_API] Unable to get data format" << std::endl;
			return false;
		}
	}
	else {
		std::cout << "[ERROR][LYA_API] LYA is not connected" << std::endl;
		return false;
	}
}