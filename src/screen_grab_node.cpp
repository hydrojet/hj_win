/*
 * Created by Simone 07/10/2019
 */

#include <ros/ros.h>
#include <sensor_msgs/Image.h>

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "cv_bridge/cv_bridge.h"

#include <iostream>
#include <windows.h>
#include <hj_win/screen_grab.h>

namespace encoding = sensor_msgs::image_encodings;

int main( int argc, char **argv ) {

	ros::init(argc, argv, "screen_grab_node");
	ros::NodeHandle n;

	sensor_msgs::Image image_msg;
	cv::Mat cvImage;
	cv::Mat cvResizedImage;
	cv_bridge::CvImage cvBridgeImage;

	float frame_rate = 0;
	int x_off = 0;
	int y_off = 0;
	int width = 0;
	int height = 0;
	int camera_res = 0;

	ros::Publisher image_pub = n.advertise<sensor_msgs::Image>("/hj_camera/image_raw", 10);

	if (n.hasParam("/update_rate") && n.hasParam("/x_offset") && n.hasParam("/y_offset") && n.hasParam("/roi_width") && n.hasParam("/roi_height") && n.hasParam("/camera_resolution")) {
		n.getParam("/update_rate", frame_rate);
		ROS_WARN("Frame Rate: %f", frame_rate);
		n.getParam("/x_offset", x_off);
		n.getParam("/y_offset", y_off);
		n.getParam("/roi_width", width);
		n.getParam("/roi_height", height);
		ROS_WARN("ROI: %d  x  %d", width, height);
		n.getParam("/camera_resolution", camera_res);
		ROS_WARN("Scale Factor: %f", (float)camera_res/(float)width);
	}
	else {
		ROS_ERROR("No ROI specified. Try using the launch file!! Terminating.");
		return -1;
	}

	ScreenGrabber screen(x_off, y_off, width, height);
	cvImage.create(height, width, CV_8UC4);

	ros::Rate loop_rate(frame_rate);

	while ( ros::ok() ) {

		screen.grabToCV(cvImage);
		cv::resize(cvImage, cvResizedImage, cv::Size(), (float)camera_res/(float)width, (float)camera_res/(float)width);
		cvBridgeImage.image = cvResizedImage;
		cvBridgeImage.toImageMsg(image_msg);
		image_msg.encoding = encoding::BGRA8;
		image_msg.header.frame_id = "hj_camera";
		image_pub.publish(image_msg);

		ros::spinOnce();

		loop_rate.sleep();
	}


	return 0;
}
