/*
 * Created by Simone 07/10/2019
 */

#include "hj_win/screen_grab.h"

ScreenGrabber::ScreenGrabber(int offset_x, int offset_y, int x_len, int y_len) {
	a.x = offset_x;
	a.y = offset_y;
	b.x = a.x + x_len;
	b.y = a.y + y_len;
	x_size = x_len;
	y_size = y_len;

	bih.biSize = sizeof(BITMAPINFOHEADER);
	bih.biWidth = x_len;
	bih.biHeight = -y_len;
	bih.biPlanes = 1;
	bih.biBitCount = 32;
	bih.biCompression = BI_RGB;
	bih.biSizeImage = 0;
	bih.biXPelsPerMeter = 0;
	bih.biYPelsPerMeter = 0;
	bih.biClrUsed = 0;
	bih.biClrImportant = 0;
}

ScreenGrabber::~ScreenGrabber() {
	// Clear Objs
	SelectObject(hDC, old_obj);
	DeleteDC(hDC);
	ReleaseDC(NULL, hScreen);
	DeleteObject(hBitmap);
}

void ScreenGrabber::grabToClipboard(){
	// Copy screen to bitmap
	hScreen = GetDC(NULL);
	hDC = CreateCompatibleDC(hScreen);
	hBitmap = CreateCompatibleBitmap(hScreen, abs(b.x - a.x), abs(b.y - a.y));
	old_obj = SelectObject(hDC, hBitmap);
	bRet = BitBlt(hDC, 0, 0, abs(b.x - a.x), abs(b.y - a.y), hScreen, a.x, a.y, SRCCOPY);

	// Save bitmap to clipboard
	OpenClipboard(NULL);
	EmptyClipboard();
	SetClipboardData(CF_BITMAP, hBitmap);
	CloseClipboard();

	// Clear Objs
	SelectObject(hDC, old_obj);
	DeleteDC(hDC);
	ReleaseDC(NULL, hScreen);
	DeleteObject(hBitmap);
}

void ScreenGrabber::grabToCV_beta(cv::Mat& matBitmap) {
	// Initialize DCs
	hScreen = GetDC(NULL); // Get DC of the target capture..
	hDC = CreateCompatibleDC(hScreen); // Create compatible DC 

	// Create hBitmap with Pointer to the pixels of the 
	ZeroMemory(&bi, sizeof(BITMAPINFO));
	bi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bi.bmiHeader.biWidth = x_size;
	bi.bmiHeader.biHeight = -y_size;  //negative so (0,0) is at top left
	bi.bmiHeader.biPlanes = 1;
	bi.bmiHeader.biBitCount = 32;

	hdc = GetDC(NULL);
	hBitmap = CreateDIBSection(hdc, &bi, DIB_RGB_COLORS, &ptrBitmapPixels, NULL, 0);

	// Set hBitmap in the hDC 
	SelectObject(hDC, hBitmap);

	// Set matBitmap to point to the pixels of the hBitmap
	matBitmap = cv::Mat(y_size, x_size, CV_8UC4, ptrBitmapPixels, 0);

	// Now update the pixels using BitBlt
	BitBlt(hDC, 0, 0, x_size, y_size, hScreen, a.x, a.y, SRCCOPY);

	DeleteDC(hDC);
	ReleaseDC(NULL, hScreen);
}

void ScreenGrabber::grabToCV_view() {
	
	cv::Mat matBitmap;

	// Initialize DCs
	hScreen = GetDC(NULL); // Get DC of the target capture..
	hDC = CreateCompatibleDC(hScreen); // Create compatible DC 

	// Create hBitmap with Pointer to the pixels of the 
	ZeroMemory(&bi, sizeof(BITMAPINFO));
	bi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bi.bmiHeader.biWidth = x_size;
	bi.bmiHeader.biHeight = -y_size;  //negative so (0,0) is at top left
	bi.bmiHeader.biPlanes = 1;
	bi.bmiHeader.biBitCount = 32;

	hdc = GetDC(NULL);
	hBitmap = CreateDIBSection(hdc, &bi, DIB_RGB_COLORS, &ptrBitmapPixels, NULL, 0);

	// Set hBitmap in the hDC 
	SelectObject(hDC, hBitmap);

	// Set matBitmap to point to the pixels of the hBitmap
	matBitmap = cv::Mat(y_size, x_size, CV_8UC4, ptrBitmapPixels, 0);

	// Now update the pixels using BitBlt
	BitBlt(hDC, 0, 0, x_size, y_size, hScreen, 0, 0, SRCCOPY);

	// Display the results through Mat
	imshow("Title", matBitmap);
}

void ScreenGrabber::grabToCV(cv::Mat& src){

	hwindowDC = GetDC(hwnd);
	hwindowCompatibleDC = CreateCompatibleDC(hwindowDC);
	SetStretchBltMode(hwindowCompatibleDC, COLORONCOLOR);
	GetClientRect(hwnd, &windowsize);

	// Create a bitmap
	hbwindow = CreateCompatibleBitmap(hwindowDC, x_size, y_size);


	// Use the previously created device context with the bitmap
	SelectObject(hwindowCompatibleDC, hbwindow);
	// Copy from the window device context to the bitmap device context
	StretchBlt(hwindowCompatibleDC, 0, 0, x_size, y_size, hwindowDC, a.x, a.y, x_size, y_size, SRCCOPY);
	GetDIBits(hwindowCompatibleDC, hbwindow, 0, y_size, src.data, (BITMAPINFO*)&bih, DIB_RGB_COLORS);

	// Avoid memory leak
	DeleteObject(hbwindow);
	DeleteDC(hwindowCompatibleDC);
	ReleaseDC(hwnd, hwindowDC);

}