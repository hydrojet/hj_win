//
// Created by Simone on 31/01/2020.
//

#include <ros/ros.h>
#include "hj_msgs/CameraSettings.h"

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <string>

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

/** Camera Settings **/
#define CONTROL_MODE 1
#define CONTROL_TARGET 2
#define CONTROL_SPEED 3
#define AWB 4
#define FPN 5
#define NOISE_REDUCTION 6
#define EDGE_ENHANCEMENT 7
#define EXPOSURE_TIME 8

#define DEFAULT_PORT "27015"
#define DEFAULT_CLIENT_PORT 55379

/** WinSock Variables **/
WSADATA wsaData;
SOCKET ConnectSocket = INVALID_SOCKET;
struct addrinfo* result = NULL, * ptr = NULL, hints;
struct sockaddr_in exphints;
char sendbuf[15] = "0*0*0\n";
int iResult;
bool ros_only = false;

void camera_settings_callback( const hj_msgs::CameraSettings::ConstPtr& msg ){
    ROS_WARN("Params %d\t%d\t%d", msg->param, msg->mode, msg->value);
    sprintf(sendbuf, "%d*%d*%d\n", msg->param, msg->mode, msg->value);
    ROS_WARN("Sending %s", sendbuf);
    if(!ros_only) {
        iResult = send(ConnectSocket, sendbuf, (int) strlen(sendbuf), 0);
        if (iResult == SOCKET_ERROR) {
            ROS_ERROR("Send failed with error: %d", WSAGetLastError());
        }
    }
}

int main(int argc, char **argv){

    /** ROS Variables **/
    std::string server_ip_address;
    std::string server_port;
    int client_port;

    /** Initialize ROS Node **/
    ros::init(argc, argv, "camera_settings_controller_node");
    ros::NodeHandle n;

    /** Create Commands Subscriber **/
    ros::Subscriber sub = n.subscribe("/camera_settings", 1, camera_settings_callback);

    /** Initialize Winsock **/
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != 0) {
        ROS_ERROR("WSAStartup failed with error: %d", iResult);
        return -1;
    }

    /** Check Server Parameters **/
    if (n.hasParam("/win_server_ip") && n.hasParam("/win_server_port") && n.hasParam("/win_client_commands_port")) {
        n.getParam("/win_server_ip", server_ip_address);
        ROS_WARN("Found ROS param /win_server_ip = %s", server_ip_address.c_str());
        n.getParam("/win_server_port", server_port);
        if(server_port.empty())
            server_port = DEFAULT_PORT;
        ROS_WARN("Found ROS param /win_server_port = %s", server_port.c_str());
        n.getParam("/win_client_commands_port", client_port);
        ROS_WARN("Found ROS param /win_client_commands_port = %d", client_port);
    }
    else{
        // Automatically retrieve HostName
        char szHostName[255];
        gethostname(szHostName, 255);
        struct hostent* host_entry;
        host_entry = gethostbyname(szHostName);
        char* szLocalIP;
        szLocalIP = inet_ntoa(*(struct in_addr*) * host_entry->h_addr_list);
        ROS_WARN("Unable to contact the Win Server without an IP address. Assuming %s\t%s - 27015", szHostName, szLocalIP);

        server_ip_address = szLocalIP;
        ROS_WARN("Using default IP = %s", server_ip_address.c_str());
        server_port = DEFAULT_PORT;
        ROS_WARN("Using default SERVER PORT = %s", server_port.c_str());
        client_port = DEFAULT_CLIENT_PORT;
        ROS_WARN("Using default CLIENT PORT = %d", client_port);
    }

    ROS_WARN("Connecting to %s - %s", server_ip_address.c_str(), server_port.c_str());

    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    /** Resolve the server address and port **/
    iResult = getaddrinfo(&server_ip_address.front(), &server_port.front(), &hints, &result);
    if (iResult != 0) {
        ROS_ERROR("Getaddrinfo failed with error: %d", iResult);
        WSACleanup();
        return -1;
    }

    /** Attempt to connect to an address until one succeeds **/
    for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {
        // Create a SOCKET for connecting to server
        ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
        if (ConnectSocket == INVALID_SOCKET) {
            ROS_ERROR("Socket failed with error: %ld", WSAGetLastError());
            WSACleanup();
            return -1;
        }
        // Binding Client on a custom and explicit port in order to be recognized by the Server
        exphints.sin_family = AF_INET;
        exphints.sin_addr.s_addr = inet_addr(server_ip_address.c_str());
        exphints.sin_port = htons(client_port);

        iResult = bind(ConnectSocket, (SOCKADDR *) &exphints, sizeof (exphints));
        if (iResult != 0) {
            ROS_ERROR("Bind failed with error: %ld", WSAGetLastError());
        }

        // Connect to server.
        iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (iResult == SOCKET_ERROR) {
            closesocket(ConnectSocket);
            ConnectSocket = INVALID_SOCKET;
            continue;
        }
        break;
    }

    freeaddrinfo(result);

    if (ConnectSocket == INVALID_SOCKET) {
        ROS_ERROR("Unable to connect to Win Server! No command will be sent!!!");
        WSACleanup();
        ros_only = true;
        //return -1;
    }

    /** Send an initial buffer **/
    if(!ros_only) {
        iResult = send(ConnectSocket, sendbuf, (int) strlen(sendbuf), 0);
        if (iResult == SOCKET_ERROR) {
            ROS_ERROR("Send failed with error: %d", WSAGetLastError());
            closesocket(ConnectSocket);
            WSACleanup();
            return -1;
        }
    }

    ROS_WARN("Communication Started");

    ros::spin();

    /** Cleanup Winsock **/
    if(!ros_only) {
        ROS_WARN("Cleaning up connection");
        closesocket(ConnectSocket);
        WSACleanup();
    }

    return 0;
}