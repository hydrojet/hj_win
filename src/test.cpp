/*
 * Created by Simone 07/10/2019
 */

#include <ros/ros.h>
#include <iostream>
#include <windows.h>
#include <hj_win/screen_grab.h>

int main(int argc, char** argv){

	ScreenGrabber screen(0, 0, 100, 100);

	while (1) {
		screen.grabToCV_view();
	}

	// Wait until some key is pressed
	cv::waitKey(0);

	screen.~ScreenGrabber();

	return 0;
}
