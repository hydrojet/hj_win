/*
* Created by Simone 31/01/2020
*/

#include <ros/ros.h>
#include <sensor_msgs/Image.h>

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "cv_bridge/cv_bridge.h"

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <cstdlib>
#include <cstdio>
#include <iostream>

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

#define IMG_ROWS 400
#define IMG_COLS 400
#define DEFAULT_BUFLEN (IMG_ROWS * IMG_COLS * 4)
#define RECV_DEFAULT_BUFLEN (IMG_ROWS * IMG_COLS * 2)
#define DEFAULT_PORT "27015"
#define DEFAULT_CLIENT_PORT 44268

namespace encoding = sensor_msgs::image_encodings;

int __cdecl main(int argc, char** argv){

    /** WinSock Variables **/
    WSADATA wsaData;
    SOCKET ConnectSocket = INVALID_SOCKET;
    struct addrinfo* result = NULL, * ptr = NULL, hints;
    struct sockaddr_in exphints;
    char sendbuf = 's';
    std::vector<uint8_t> recvbuf(RECV_DEFAULT_BUFLEN);
    int iResult;

    /** ROS Variables **/
    sensor_msgs::Image image_msg;
    std::string server_ip_address;
    std::string server_port;
    int client_port;

    /** Initialize ROS Node **/
    ros::init(argc, argv, "camera_publisher_node");
    ros::NodeHandle n;

    /** Create Image Publisher **/
    ros::Publisher image_pub = n.advertise<sensor_msgs::Image>("/hj_camera/image_raw", 10);

    /** Initialize Winsock **/
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != 0) {
        ROS_ERROR("WSAStartup failed with error: %d", iResult);
        return -1;
    }

    /** Check Server Parameters **/
    if (n.hasParam("/win_server_ip") && n.hasParam("/win_server_port") && n.hasParam("/win_client_image_port")) {
        n.getParam("/win_server_ip", server_ip_address);
        ROS_WARN("Found ROS param /win_server_ip = %s", server_ip_address.c_str());
        n.getParam("/win_server_port", server_port);
        if(server_port.empty())
            server_port = DEFAULT_PORT;
        ROS_WARN("Found ROS param /win_server_port = %s", server_port.c_str());
        n.getParam("/win_client_image_port", client_port);
        ROS_WARN("Found ROS param /win_client_image_port = %d", client_port);

    }
    else{// Automatically retrieve HostName
        char szHostName[255];
        gethostname(szHostName, 255);
        struct hostent* host_entry;
        host_entry = gethostbyname(szHostName);
        char* szLocalIP;
        szLocalIP = inet_ntoa(*(struct in_addr*) * host_entry->h_addr_list);
        ROS_WARN("Unable to contact the Win Server without an IP address. Assuming %s\t%s - 27015", szHostName, szLocalIP);

        server_ip_address = szLocalIP;
        ROS_WARN("Using default IP = %s", server_ip_address.c_str());
        server_port = DEFAULT_PORT;
        ROS_WARN("Using default SERVER PORT = %s", server_port.c_str());
        client_port = DEFAULT_CLIENT_PORT;
        ROS_WARN("Using default CLIENT PORT = %d", client_port);
    }

    ROS_WARN("Connecting to %s - %s", server_ip_address.c_str(), server_port.c_str());

    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    /** Resolve the server address and port **/
    iResult = getaddrinfo(&server_ip_address.front(), &server_port.front(), &hints, &result);
    if (iResult != 0) {
        ROS_ERROR("Getaddrinfo failed with error: %d", iResult);
        WSACleanup();
        return -1;
    }

    /** Attempt to connect to an address until one succeeds **/
    for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {
        // Create a SOCKET for connecting to server
        ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
        if (ConnectSocket == INVALID_SOCKET) {
            ROS_ERROR("Socket failed with error: %ld", WSAGetLastError());
            WSACleanup();
            return -1;
        }
        // Binding Client on a custom and explicit port in order to be recognized by the Server
        exphints.sin_family = AF_INET;
        exphints.sin_addr.s_addr = inet_addr(server_ip_address.c_str());
        exphints.sin_port = htons(client_port);

        iResult = bind(ConnectSocket, (SOCKADDR *) &exphints, sizeof (exphints));
        if (iResult != 0) {
            ROS_ERROR("Bind failed with error: %ld", WSAGetLastError());
            return -1;
        }
        // Connect to server.
        iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (iResult == SOCKET_ERROR) {
            closesocket(ConnectSocket);
            ConnectSocket = INVALID_SOCKET;
            continue;
        }
        break;
    }

    freeaddrinfo(result);

    if (ConnectSocket == INVALID_SOCKET) {
        ROS_ERROR("Unable to connect to Win Server!");
        WSACleanup();
        return -1;
    }

    /** Send an initial buffer **/
    iResult = send(ConnectSocket, &sendbuf, 1, 0);
    if (iResult == SOCKET_ERROR) {
        ROS_ERROR("Send failed with error: %d", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
        return -1;
    }

    ROS_WARN("Communication Started");

    /** Shutdown the connection since no more data will be sent by the Client **/
    iResult = shutdown(ConnectSocket, SD_SEND);
    if (iResult == SOCKET_ERROR) {
        ROS_ERROR("Shutdown failed with error: %d", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
        return -1;
    }

    /** Receive until the peer closes the connection **/
    do {
        iResult = recv(ConnectSocket, (char *) &recvbuf.front(), recvbuf.size(), 0);
        if (iResult > 0) {
            image_msg.header.frame_id = "hj_camera";
            image_msg.height = IMG_ROWS;
            image_msg.width = IMG_COLS;
            image_msg.encoding = encoding::YUV422;
            image_msg.is_bigendian = false;
            image_msg.step = IMG_ROWS * 2;
            image_msg.data = recvbuf;
            image_pub.publish(image_msg);
        }
        else if (iResult == 0)
            ROS_WARN("Connection closed");
        else
            ROS_ERROR("Recv failed with error: %d", WSAGetLastError());

    } while (iResult > 0);

    // Cleanup
    closesocket(ConnectSocket);
    WSACleanup();

    return 0;
}
